"""Create a CloudFormation template and upload it to AWS."""
import click
import json
import random
import string
from create import make_template
from util import create_boto_session, get_stack_output, zip_code


def deploy(
    project_name=None,
    stage='v1',
    verbose=False,
    zip_file_name='index.zip',
):
    """Create the CloudFormation template and upload to AWS."""
    session = create_boto_session()

    bucket_name = "{}-{}".format(
        project_name.lower(),
        ''.join(random.choices(string.ascii_lowercase, k=16))
    )
    client = session.client('s3')
    client.create_bucket(Bucket=bucket_name)
    zip_code(
        bucket_name=bucket_name,
        zip_file_name=zip_file_name,
        path='code'
    )

    client = session.client('s3')
    click.echo('Uploading code to S3...', nl=False)
    response = client.upload_file(zip_file_name, bucket_name, zip_file_name)
    click.secho(' done', fg='green')

    template = make_template(
        api_name="{}API".format(project_name),
        role_name="{}Role".format(project_name),
        function_name="{}Function".format(project_name),
        resource_name="{}Resource".format(project_name),
        usage_plan_name="{}UsagePlan".format(project_name),
        usage_plan_key="{}UsagePlanKey".format(project_name),
        zip_file_name=zip_file_name,
        bucket_name=bucket_name
    )
    if verbose:
        click.echo(template.to_json())
        exit(1)

    stack_name = '{}Stack'.format(project_name)

    resource = session.resource('cloudformation')
    resource.create_stack(
        StackName=stack_name,
        TemplateBody=template.to_json(),
        Capabilities=['CAPABILITY_NAMED_IAM']
    )

    output = get_stack_output(stack_name, session)

    client = session.client('apigateway')

    response = client.get_api_key(apiKey=output['ApiKey'], includeValue=True)

    output['ApiKeyValue'] = response['value']

    output['profile_name'] = session.profile_name

    output['profile_region'] = session.region_name

    output['project_name'] = project_name

    output['BucketName'] = bucket_name

    config = {stage: output}

    with open('config.json', 'w') as file:
        file.write(json.dumps(config, indent=4))

    return config


if __name__ == "__main__":
    deploy()
