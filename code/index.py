"""Return 'Alive' if working."""
import json


def handler(event, context):
    """Return 'Alive' if working."""
    body = {"status": "Alive"}
    return {
        "statusCode": 200,
        "headers": {"Content-Type": "application/json"},
        "body": json.dumps(body)
    }


if __name__ == '__main__':
    result = handler(None, None)
    print(json.dumps(result, indent=4))
