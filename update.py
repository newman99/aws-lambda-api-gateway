"""Update Lambda function."""
import json
import os
import boto3
import click
from util import zip_code


def update(
    bucket_name='example-bucket',
    file_name="index.py",
    function_name='ExampleFunction',
    path="code",
    profile_name='default',
    stage_name="v1",
    zip_file_name="index.zip"
):
    """Update Lambda function."""
    zip_code(
        bucket_name=bucket_name,
        zip_file_name='index.zip',
        path=path
    )

    session = boto3.Session(profile_name=profile_name)
    client = session.client('s3')
    click.echo('Uploading code to S3...', nl=False)
    response = client.upload_file(zip_file_name, bucket_name, zip_file_name)
    click.secho(' done', fg='green')

    client = session.client('lambda')

    response = client.update_function_code(
        FunctionName=function_name,
        S3Bucket=bucket_name,
        S3Key=zip_file_name
    )

    print(json.dumps(response, indent=4))

    os.remove(zip_file_name)


if __name__ == "__main__":
    update()
