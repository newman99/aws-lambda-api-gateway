"""Create boto session."""
import botocore
import boto3
import click
import os
import time
from zipfile import ZipFile


def create_boto_session():
    """Create boto session."""
    session = botocore.session.Session()
    config = session.full_config
    profiles = config.get('profiles', {})
    profile_names = list(profiles.keys())

    if not profile_names:
        click.echo('Error: Set up the ~/.aws/credentials file.')
        exit(1)
    elif len(profile_names) == 1:
        profile_name = profile_names[0]
        click.echo("Okay, using profile {}!".format(
            click.style(profile_name, bold=True))
        )
    else:
        if "default" in profile_names:
            default_profile = [p for p in profile_names if p == "default"][0]
        else:
            default_profile = profile_names[0]
        while True:
            profile_name = input(
                "We found the following profiles: {}, and {}. "
                "Which would you like us to use? (default '{}'): "
                .format(
                     ', '.join(profile_names[:-1]),
                     profile_names[-1],
                     default_profile
                 )) or default_profile
            if profile_name in profiles:
                break

    session = boto3.Session(profile_name=profile_name)

    return session


def zip_code(
    zip_file_name='index.zip',
    path='code',
    bucket_name=None
):
    """Zip code and upload to S3."""
    with ZipFile(zip_file_name, mode='w') as zipfile:
        for root, dirs, files in os.walk(path):
            for file in files:
                file_path = os.path.join(root, file)
                zipfile.write(file_path, file_path[len(path):])

    return zip_file_name


def get_stack_output(stack_name, session):
    """Get the AWS API Gateway host."""
    client = session.client('cloudformation')
    stack_status = None
    click.echo("Waiting for stack creation..", nl=False)
    while stack_status != 'CREATE_COMPLETE':
        click.echo(".", nl=False)
        time.sleep(10)
        response = client.describe_stacks(
            StackName=stack_name
        )
        stack_status = response['Stacks'][0]['StackStatus']
        if stack_status == 'ROLLBACK_COMPLETE':
            click.echo('Error - Stack creation failed.')
            exit(1)
    click.secho(' done', fg='green')
    output = {}
    for item in response['Stacks'][0]['Outputs']:
        output[item['OutputKey']] = item['OutputValue']

    return output
