# AWS Lambda with API Gateway

This project allows you to deploy and AWS Lambda function with an API Gateway trigger.
It also creates a local development environment using Docker.

This project is built using the following tools and libraries:
* [AWS](https://aws.amazon.com/) - Amazon Web Services
  * [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html) - manage your AWS services from the command line
  * [CloudFormation](https://aws.amazon.com/cloudformation/) - model and provision all your cloud infrastructure resources
  * [Lambda](https://aws.amazon.com/lambda/) - run code without provisioning or managing servers
  * [API Gateway](https://aws.amazon.com/api-gateway/) - deliver robust, secure, and scalable mobile and web application backends
  * [S3](https://aws.amazon.com/s3/) - object storage built to store and retrieve any amount of data from anywhere
* [botocore](https://github.com/boto/botocore) - the low-level, core functionality of boto 3
* [boto3](https://github.com/boto/boto3) - AWS SDK for Python
* [troposphere](https://github.com/cloudtools/troposphere) - Python library to create AWS CloudFormation descriptions
* [Docker](https://www.docker.com/) - a computer program that performs operating-system-level virtualization
* [Docker Compose](https://docs.docker.com/compose/install/) - a tool for defining and running multi-container Docker applications
* [Click](https://click.palletsprojects.com/en/7.x/) - a Python package for creating beautiful command line interfaces
* [Git](https://git-scm.com/) - version control (optional)


## Prerequisites

This project has only been tested with [Python 3](https://www.python.org/download/releases/3.0/).

An [AWS](https://aws.amazon.com/) account is required.
Install and configure [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html).

Install [Docker](https://www.docker.com/get-started) and [Docker Compose](https://docs.docker.com/compose/install/).

Install awacs, boto3, botocore, click, docker, urllib3, and troposphere using pip:

```bash
pip install awacs boto3 botocore click docker urllib3 troposphere
```

Install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) (optional).

## Installing

Download this project:

```bash
wget https://gitlab.com/newman99/aws-lambda-api-gateway/-/archive/master/aws-lambda-api-gateway-master.zip
```

Unzip to the directory of your choice:

```bash
unzip aws-lambda-api-gateway-master.zip
```

Change the name of the unzipped directory to the name of your project:

```bash
mv aws-lambda-api-gateway-master.zip project_name
```

Navigate to the new directory:

```bash
cd project_name
```

Initialize git repository (optional):

```bash
git init
```

## Deployment

Run the setup:

```bash
python3 cli.py deploy stage
```

Your new CloudFormation Stack will be ready in about 2 minutes.

## Local Docker development

Test the Lambda function locally using Docker Compose:

```bash
docker-compose run lambda python index.py
```

## Authors

* **Matthew Newman**

## License

This project is licensed under The Unlicense - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

[Building a Serverless Image Resizer](https://deployeveryday.com/2017/07/25/building-image-resizer-serverless.html)
