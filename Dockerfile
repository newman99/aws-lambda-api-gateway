FROM lambci/lambda:build-python3.6

ENV PYTHONUNBUFFERED 1

MAINTAINER "Matthew Newman" <newman99@gmail.com>

WORKDIR /var/task

# Additional RUN commands here
# RUN yum clean all && \
#    yum -y install <stuff>
#RUN pip install --upgrade pip
#$RUN pip install -U boto3 awscli

CMD ["bash"]
