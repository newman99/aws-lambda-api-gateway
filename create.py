"""Create Template."""
from troposphere import Output, Ref, Template
from troposphere.apigateway import RestApi, Method
from troposphere.apigateway import Resource, MethodResponse
from troposphere.apigateway import Integration, IntegrationResponse
from troposphere.apigateway import Deployment, Stage, ApiStage
from troposphere.apigateway import UsagePlan, QuotaSettings, ThrottleSettings
from troposphere.apigateway import ApiKey, StageKey, UsagePlanKey
from troposphere.awslambda import Function, Code
from troposphere.iam import Role, Policy
from troposphere import GetAtt, Join


def make_template(
    file_name="code/index.py",
    role_name="LambdaExecutionRole",
    api_name="ExampleApi",
    bucket_name="ExampleS3",
    function_name="ExampleFunction",
    resource_name="ExampleResource",
    path_name="alive",
    region="us-east-1",
    runtime="python3.6",
    stage_name='v1',
    usage_plan_name="ExampleUsagePlan",
    usage_plan_key="ExampleUsagePlanKey",
    zip_file_name="index.zip"
        ):
    """Make CloudFormation template for Lambda and API Gateway."""
    t = Template()

    t.add_description("Lambda function with API Gateway trigger.")

    # Create the Api Gateway
    rest_api = t.add_resource(RestApi(
        api_name,
        Name=api_name
    ))

    # Create a role for the lambda function
    t.add_resource(Role(
        role_name,
        Path="/",
        Policies=[Policy(
            PolicyName="root",
            PolicyDocument={
                "Version": "2012-10-17",
                "Statement": [{
                    "Action": ["logs:*"],
                    "Resource": "arn:aws:logs:*:*:*",
                    "Effect": "Allow"
                }, {
                    "Action": ["lambda:*"],
                    "Resource": "*",
                    "Effect": "Allow"
                }, {
                    "Action": ["s3:*"],
                    "Resource": "*",
                    "Effect": "Allow"
                }]
            })],
        AssumeRolePolicyDocument={"Version": "2012-10-17", "Statement": [
            {
                "Action": ["sts:AssumeRole"],
                "Effect": "Allow",
                "Principal": {
                    "Service": [
                        "lambda.amazonaws.com",
                        "apigateway.amazonaws.com"
                    ]
                }
            }
        ]},
    ))

    # Create the Lambda function
    function = t.add_resource(Function(
        function_name,
        Code=Code(
            S3Bucket=bucket_name,
            S3Key=zip_file_name
        ),
        Handler="index.handler",
        Role=GetAtt(role_name, "Arn"),
        Runtime="python3.6",
    ))

    # Create a resource to map the lambda function to
    resource = t.add_resource(Resource(
        resource_name,
        RestApiId=Ref(rest_api),
        PathPart=path_name,
        ParentId=GetAtt(api_name, "RootResourceId"),
    ))

    # Create a Lambda API method for the Lambda resource
    t.add_resource(Method(
        "LambdaMethod",
        ApiKeyRequired=True,
        DependsOn=function_name,
        RestApiId=Ref(rest_api),
        AuthorizationType="NONE",
        ResourceId=Ref(resource),
        HttpMethod="GET",
        Integration=Integration(
            Credentials=GetAtt(role_name, "Arn"),
            Type="AWS_PROXY",
            IntegrationHttpMethod='POST',
            IntegrationResponses=[
                IntegrationResponse(
                    StatusCode='200'
                )
            ],
            Uri=Join("", [
                "arn:aws:apigateway:",
                region,
                ":lambda:path/2015-03-31/functions/",
                GetAtt(function_name, "Arn"),
                "/invocations"
            ])
        ),
        MethodResponses=[
            MethodResponse(
                "CatResponse",
                StatusCode='200'
            )
        ]
    ))

    deployment = t.add_resource(Deployment(
        "{}Deployment".format(stage_name),
        DependsOn="LambdaMethod",
        RestApiId=Ref(rest_api),
    ))

    stage = t.add_resource(Stage(
        "{}Stage".format(stage_name),
        StageName=stage_name,
        RestApiId=Ref(rest_api),
        DeploymentId=Ref(deployment)
    ))

    key = t.add_resource(ApiKey(
        "ApiKey",
        DependsOn="{}Deployment".format(stage_name),
        Enabled=True,
        StageKeys=[StageKey(
            RestApiId=Ref(rest_api),
            StageName=Ref(stage)
        )]
    ))

    # Create an API usage plan
    usagePlan = t.add_resource(UsagePlan(
        usage_plan_name,
        UsagePlanName=usage_plan_name,
        Description="Example usage plan",
        Quota=QuotaSettings(
            Limit=50000,
            Period="MONTH"
        ),
        Throttle=ThrottleSettings(
            BurstLimit=500,
            RateLimit=5000
        ),
        ApiStages=[
            ApiStage(
                ApiId=Ref(rest_api),
                Stage=Ref(stage)
            )]
    ))

    # tie the usage plan and key together
    t.add_resource(UsagePlanKey(
        usage_plan_key,
        KeyId=Ref(key),
        KeyType="API_KEY",
        UsagePlanId=Ref(usagePlan)
    ))

    # Add the deployment endpoint as an output
    t.add_output([
        Output(
            'FunctionName',
            Value=Ref(function),
            Description="Lambda function name"
        ),
        Output(
            "ApiEndpoint",
            Value=Join("", [
                "https://",
                Ref(rest_api),
                ".execute-api.",
                region,
                ".amazonaws.com/",
                stage_name,
                "/",
                path_name
            ]),
            Description="Endpoint for this stage of the api"
        ),
        Output(
            "ApiKey",
            Value=Ref(key),
            Description="API key"
        ),
        Output(
            "StageName",
            Value=stage_name,
            Description="Stage name"
        ),
        Output(
            "RoleName",
            Value=role_name,
            Description="Role name"
        ),
        Output(
            "Runtime",
            Value=runtime,
            Description="Lambda runtime"
        )
    ])

    return t


if __name__ == '__main__':
    make_template()
