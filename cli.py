"""Command line interface."""
import json
import re
import click
from deploy import deploy
from update import update


def validate_project_name(value):
    """Validate project name - only letters and numbers."""
    if not re.match(r"^[a-zA-Z0-9]+$", value):
        raise click.BadParameter(
            '[{}] only letters and numbers are allowed.'.format(value)
        )
    return value


def validate_operation(ctx, param, value):
    """Validate operation."""
    operations = ['deploy', 'update']
    if value in operations:
        return value
    click.echo("Undefined operation type: {}. \
The supported operations are {}.".format(value, ", ".join(operations)))
    exit(1)


@click.command()
@click.argument('operation', callback=validate_operation)
@click.argument('stage')
@click.option('-v', '--verbose', is_flag=True, show_default=True,
              help='Verbose.')
def cli(operation, stage, verbose):
    """Command line interface."""
    if operation == "deploy":
        project_name = click.prompt('Enter your project name', type=str)
        validate_project_name(project_name)
        config = deploy(
            project_name=project_name,
            stage=stage,
            verbose=verbose
        )
    elif operation == "update":
        with open('config.json') as file:
            config = json.load(file)
        update(
            bucket_name=config[stage]['BucketName'],
            function_name=config[stage]['FunctionName'],
            profile_name=config[stage]['profile_name']
        )
    else:
        click.echo("Operation not found: {}".format(operation))
        exit(1)

    click.echo("\nThe Lambda function is named: {}".format(
        config[stage]['FunctionName']))

    click.echo("\nThe API is ready at {}\n\nThe API key is {}".format(
        config[stage]['ApiEndpoint'], config[stage]['ApiKeyValue']))

    click.echo('\nTest the API endpoint using: \
curl -X GET -H "X-Api-Key: {}" -w "\\n" {}'.format(
        config[stage]['ApiKeyValue'],
        config[stage]['ApiEndpoint']
    ))

    exit(0)


if __name__ == "__main__":
    cli()
